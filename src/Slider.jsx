import { Component } from "react";
import ReactDOM from 'react-dom';
import App from './App';
import './slider.css'

let json = []

class Project extends Component {

    handleClick () {
        ReactDOM.render(
              <App />,
            document.getElementById('root')
          );
    }

    render () {
        return <div className="box-project">
            <a href="#" onClick={this.handleClick} className="close"></a>
            <div className="thisimg">
            <Slider  projectArray={json[this.props.project].imgs} nbrSlide={3} c={false} />
            </div>
            <h2>{json[this.props.project].title}</h2>
            <div className="thetitle">
            <h3 className="context">Contexte</h3>
            <h3>Compétences travaillés</h3>
            </div>
            <div className="info">
                <p>{json[this.props.project].contexte}</p>
                <div className="skills-ul">
                    <ul> {json[this.props.project].competencetitle1}
                        <li>{json[this.props.project].competencecontent11}</li>
                        <li>{json[this.props.project].competencecontent12}</li>
                        <li>{json[this.props.project].competencecontent13}</li>
                    </ul>
                    <ul> {json[this.props.project].competencetitle2}
                        <li>{json[this.props.project].competencecontent21}</li>
                        <li>{json[this.props.project].competencecontent22}</li>
                        <li>{json[this.props.project].competencecontent23}</li>
                    </ul>
                    <ul> {json[this.props.project].competencetitle3}
                        <li>{json[this.props.project].competencecontent31}</li>
                        <li>{json[this.props.project].competencecontent32}</li>
                        <li>{json[this.props.project].competencecontent33}</li>
                    </ul>
                    <ul> {json[this.props.project].competencetitle4}
                        <li>{json[this.props.project].competencecontent41}</li>
                        <li>{json[this.props.project].competencecontent42}</li>
                        <li>{json[this.props.project].competencecontent43}</li>
                    </ul>
                </div>
            </div>
            <div className="btn">
                <a href="#" className="blue-btn">Durée : {json[this.props.project].duree}</a>
                <a href={json[this.props.project].site} target="_blank" className="blue-btn">Plus d'informations...</a>
                <a href={json[this.props.project].github} target="_blank" className="blue-btn"><img src="./img/github.svg" alt="github" /> Github</a>
            </div>
        </div>
    }
}

function handleBox(i, c) {
    i = parseFloat(i)
    if (c) {
        ReactDOM.render(<Project project={i} />, document.getElementById('root'))
    }
}

function BoxSlide({url, project, c}) {
    if (url === null) {
        return <div className="box-slide"></div>
    }
    return <div className="box-slide">
        <button onClick={() => window.open(url)} className="zoom-icon"><span class="iconify" data-icon="zmdi:zoom-in"></span></button>
      <div className="container-img" onClick={() => handleBox(project, c)}>
        <img  src={url} alt="img" />
        <div class="text-block">
            <h4>Cliquez</h4>
            <p>pour voir le projet</p>
        </div>
        
    </div>
    </div>
}


export class Slider extends Component {
    constructor (props) {
        super(props)
        json = this.props.projectArray
        this.state = {
            slide: [],
            nbrSlide: this.props.nbrSlide,
            firstSlide: 0
        }
        this.slider = this.slider.bind(this)
        this.previous = this.previous.bind(this)
        this.next = this.next.bind(this)
    }

    componentDidMount() {
        this.slider(this.state.firstSlide)
    }

    slider (n) {
        let array = []
        let key = this.state.firstSlide
        for (let i = n; i < n + this.state.nbrSlide; i++) {
            key++
            if (i >= this.props.projectArray.length || i === -1) {
                array.push(<BoxSlide url={null} key={key} />)
            } else {
                array.push(<BoxSlide className="t" project={i} url={this.props.projectArray[i].url} key={key} c={this.props.c} />)
            }
        }
        this.setState({
            slide: array,
            firstSlide: n
        })
    }

    
    previous () {
        const n = parseFloat(this.state.firstSlide) - 1
        if (n >= -1) {
            this.slider(n)
        }
    }

    next () {
        const n = parseFloat(this.state.firstSlide) + 1
        if (n < this.props.projectArray.length - 1) {
            this.slider(n)
        }
    }

    render () {
        return <div className="slidershow">
            <img onClick={this.previous} className="arrow-slide" src="./img/arrow.svg" alt="arrow" />
            <div className="boxs">
                {this.state.slide}
            </div>
            <img onClick={this.next} className="arrow-slide" src="./img/arrow.svg" alt="arrow" />
        </div>
    }
}