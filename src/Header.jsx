import { Component } from "react";
import './Header.css'

export class Header extends Component {
    render () {
        return <>
            <header>
                <div className="header-ban">
                    <div className="banner">
                        <div className="round"></div>
                        <h1>UI Design & Code</h1>
                    </div>
                </div>
                <nav className="rs">
                    <a href="https://twitter.com/SergenYuksel01" target="_blank"><img src="./img/twitter.svg" alt="twitter" /></a>
                    <a href="https://www.linkedin.com/in/sergen-yuksel-1a7113203/" target="_blank"><img src="./img/linkedin.svg" alt="linkedin" /></a>
                    <a href="https://github.com/SergenYUKSEL" target="_blank"><img src="./img/github.svg" target="_blank" alt="github" /></a>
                </nav>
            </header>
        </>
    }
} 