import { Component } from "react";
import './skills.css'


class SkillBox extends Component {
    render () {
        return <div className="skill-box">
        <img src={this.props.url} alt="skill" />
            <div className="info">
                <h6>{this.props.skillName}</h6>
                <p>{this.props.description}</p>
            </div>
            <img src="./img/arrow.png" alt="arrow" className="blue-arrow" />
        </div>
    }
}

export class Skills extends Component {

    constructor (props) {
        super(props)
        this.state = {
            skillArray: [
                {url: "./img/vue.png", skillName: "Vue.js", description: "Vue.js, est un framework JavaScript open-source utilisé pour construire des interfaces utilisateur et des applications web monopages."},
                {url: "./img/React.png", skillName: "React", description: "React est une bibliothèque JavaScript libre développée par Facebook depuis 2013. Le but principal de cette bibliothèque est de faciliter la création d'application web monopage, via la création de composants dépendant d'un état et générant une page HTML à chaque changement d'état."},
                {url: "./img/nodejs.png", skillName: "Node JS", description: "Node.js est une plateforme logicielle libre en JavaScript, orientée vers les applications réseau évènementielles hautement concurrentes qui doivent pouvoir monter en charge."},
                {url: "./img/express.png", skillName: "Express", description: "Express.js est un framework pour construire des applications web basées sur Node.js. C'est de fait le framework standard pour le développement de serveur en Node.js"},
                {url: "./img/bootstrap.svg", skillName: "Bootstrap", description: "Bootstrap est une collection d'outils utiles à la création du design de sites et d'applications web. C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons, outils de navigation et autres éléments interactifs, ainsi que des extensions JavaScript en option."},
                {url: "./img/bulma.png", skillName: "Bulma", description: "Bulma est un framework CSS libre et open source qui fournit des composants frontaux prêts à l'emploi que vous pouvez facilement combiner pour créer des interfaces web réactives."},
            ],
            nbrSkillShow: 3,
            nbrSkill: this.props.length
        }
        this.moreSkill = this.moreSkill.bind(this)
    }


    moreSkill (e) {
        e.preventDefault()
        if (this.state.nbrSkillShow === 3) {
            this.setState({nbrSkillShow: this.state.nbrSkill})
            e.target.innerText = "Moins..."
        } else {
            this.setState({nbrSkillShow: 3})
            e.target.innerText = "Plus..."
        }
    }

    render () {
        let skills = []
        this.state.skillArray.forEach((skill, k) => {
            if (k < this.state.nbrSkillShow) {
                skills.push(<SkillBox url={skill.url} skillName={skill.skillName} description={skill.description} key={k} />)
            }
        })
        return <>
            <div className="skills-container">
                <h3>Frameworks utilisés</h3>
                {skills}
                <a href="#" onClick={this.moreSkill} className="blue-btn">Plus...</a>
            </div>
        </>
    }
} 