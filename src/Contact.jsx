import { Component } from "react";
import "./contact.css"

function Input({labelText, name, type}) {
    return <div className={name}>
        <label htmlFor={name}>{labelText}</label>
        <input type={type} id={name} placeholder={labelText} />
     </div>
}

function Select() {
    return <div className="subject">
        <label htmlFor="subject">Subject</label>
        <select name="subject" id="subject">
            <option value="">--Please choose--</option>
            <option value="freelance">Freelance</option>
            <option value="talk">Let's talk</option>
            <option value="other">Other</option>
        </select>
    </div>
}

function MessageTextArea() {
    return <div className="msg">
        <label htmlFor="msg">Message</label>
        <textarea id="msg" name="msg" placeholder={"Write down"}>
        </textarea>
    </div>
}

export class Contact extends Component {
    render () {
        return <div className="contact" id="contact">
            <h2>Contact</h2>
            <p>Say Hi, or business inqueries</p>
            <form>
                <Input labelText={"First name"} name={"firstName"} type={"text"} />
                <Input labelText={"Last name"} name={"lastName"} type={"text"} />
                <Input labelText={"Email"} name={"mail"} type={"email"} />
                <Select />
                <MessageTextArea />
            </form>
            <a href="#" className="blue-btn">Envoyer</a>
        </div>
    }
}