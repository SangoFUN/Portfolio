import { Component } from "react";
import "./footer.css"

export class Footer extends Component {
    render () {
        return <footer>
            <h1>S</h1>
            <div>
                <nav className="navigation">
                    <ul>
                        <li><a href="#" >Travaux</a></li>
                        <li><a href="./CV-Sergen.pdf" download>CV</a></li>
                        <li><a href="#" >Contact</a></li>
                    </ul>
                </nav>
                <p>Want to contact me directly by email ?</p>
                <h6>yukselsergen1@gmail.com</h6>
            </div>
            <div className="info">
                <div className="copyright">
                    <p>	&#169; 2022 Yuksel Sergen.</p>
                    <p>All Right Reserved.</p>
                </div>
                <nav className="rs">
                    <a href="https://twitter.com/SergenYuksel01" target="_blank"><img src="./img/twitter.svg" alt="twitter" /></a>
                    <a href="https://www.linkedin.com/in/sergen-yuksel-1a7113203/" target="_blank"><img src="./img/linkedin.svg" alt="linkedin" /></a>
                    <a href="https://github.com/SergenYUKSEL" target="_blank"><img src="./img/github.svg" alt="github" /></a>
                </nav>
            </div>
        </footer>
    }
}